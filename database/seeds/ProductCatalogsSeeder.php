<?php

use Illuminate\Database\Seeder;

class ProductCatalogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'product_catalogs';
        //manual geundam seed
        $name = 'Kerajinan Imitasi';
        DB::table($table)->insert([
            'name' => $name,
            'slug' => str_slug($name)
          ]);

        $name = 'Kerajinan Batu Akik';
        DB::table($table)->insert([
            'name' => $name,
            'slug' => str_slug($name)
          ]);

        $name = 'Konveksi';
        DB::table($table)->insert([
            'name' => $name,
            'slug' => str_slug($name)
          ]);

        $name = 'Kerajinan Patung';
        DB::table($table)->insert([
            'name' => $name,
            'slug' => str_slug($name)
          ]);

        $name = 'Perikanan';
        DB::table($table)->insert([
            'name' => $name,
            'slug' => str_slug($name)
          ]);
          
    }
}
