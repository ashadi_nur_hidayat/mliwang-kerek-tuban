<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = config('auth.table');
        DB::table($table)->insert([
          'username'   => 'SA',
          'password'   => bcrypt('aman123'),
          'realname'   => 'Super Administrator',
          'email'      => 'localhost@domain.web.id',
          'created_at' => '2015-07-01 09:10:09'
        ]);
    }
}
