<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'articles';
        DB::table($table)->insert([
          'category_id' => 3,
          'user_id'     => 1,
          'slug'        => str_slug('Sejarah KKN'),
          'title'       => 'Sejarah KKN',
          'keywords'    => 'KKN,PPM,UGM,2015',
          'contents'    => '<b>Kuliah Kerja Nyata</b> (KKN) adalah bentuk kegiatan pengabdian kepada masyarakat oleh mahasiswa dengan pendekatan lintas keilmuan dan sektoral pada waktu dan daerah tertentu. Pelaksanaan kegiatan KKN biasanya berlangsung antara satu sampai dua bulan dan bertempat di daerah setingkat desa. Direktorat Jenderal Pendidikan Tinggi di Indonesia telah mewajibkan setiap perguruan tinggi untuk melaksanakan KKN sebagai kegiatan intrakurikuler yang memadukan tri dharma perguruan tinggi yaitu: pendidikan, penelitian, dan pengabdian kepada masyarakat',
          'created_at'  => '2015-07-01 9:9:9',
          'updated_at'  => '2015-07-22 6:6:6'
        ]);
    }
}
