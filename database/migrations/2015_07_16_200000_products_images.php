<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Pivot table for Products and Images
*/
class ProductsImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_images', function (Blueprint $table){
          $table->increments('id');
          $table->integer('product_id')->unsigned();
          $table->integer('image_id')->unsigned();
          $table->timestamps();

          $table->foreign('product_id')->references('id')->on('products');
          $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products_images');
    }
}
