<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProducersProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producers_products', function(Blueprint $table){
          $table->increments('id');
          $table->integer('producer_id')->unsigned();
          $table->integer('product_id')->unsigned();
          $table->timestamps();

          $table->foreign('producer_id')->references('id')->on('producers');
          $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('producers_products');
    }
}
