<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table){
          $table->increments('id');
          $table->integer('catalog_id')->unsigned();
          $table->string('slug')->unique();
          $table->string('name');
          $table->mediumText('description');
          $table->timestamps();

          $table->foreign('catalog_id')->references('id')->on('product_catalogs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
