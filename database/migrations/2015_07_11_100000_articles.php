<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Articles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('articles', function (Blueprint $table){
         $table->increments('id');
         $table->integer('category_id')->unsigned();
         $table->integer('user_id')->unsigned();
         $table->string('slug')->unique();
         $table->string('title');
         $table->string('keywords')->nullable();
         $table->mediumText('contents');
         $table->integer('intro_image')->unsigned()->nullable();
         $table->timestamps();

         $table->foreign('user_id')->references('id')->on('users');
         $table->foreign('category_id')->references('id')->on('article_categories');
         $table->foreign('intro_image')->references('id')->on('images');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
