<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Images extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function(Blueprint $table){
          $table->increments('id');
          $table->string('name')->unique();
          $table->string('description')->nullable();
          $table->string('image_path')->unique();
          $table->string('mime');
          // $table->integer('width')->unsigned();
          // $table->integer('height')->unsigned();
          $table->integer('filesize')->unsigned();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');
    }
}
