<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Password setidaknya 6 karakter dan konfirmasi password cocok.',
    'user' => "Kami tidak dapat menemukan pengguna dengan e-mail tersebut.",
    'token' => 'Token password tidak ditemukan.',
    'sent' => 'URL untuk reset password telah kami kirim ke e-mail anda!',
    'reset' => 'Password telah di-reset!',

];
