<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute harus diterima.',
    'active_url'           => ':attribute bukanlah URL yang valid.',
    'after'                => ':attribute harus setelah tanggal :date.',
    'alpha'                => ':attribute hanya boleh berisi karakter.',
    'alpha_dash'           => ':attribute hanya boleh berisi karakter, angka, dan tanda kurang.',
    'alpha_num'            => ':attribute hanya boleh berisi karakter dan angka.',
    'array'                => ':attribute harus berisi array.',
    'before'               => ':attribute hanya boleh sebelum :date.',
    'between'              => [
        'numeric' => ':attribute harus di antara :min dan :max.',
        'file'    => ':attribute harus di antara :min dan :max KB.',
        'string'  => ':attribute harus di antara :min dan :max karakter.',
        'array'   => ':attribute harus berjumlah antara :min and :max item.',
    ],
    'boolean'              => ':attribute harus berisi ya/tidak.',
    'confirmed'            => ':attribute konfirmasi tidak cocok.',
    'date'                 => ':attribute bukan tanggal yang valid.',
    'date_format'          => ':attribute tidak sesuai dengan format :format.',
    'different'            => ':attribute dan :other harus berbeda.',
    'digits'               => ':attribute harus memiliki :digits digit.',
    'digits_between'       => ':attribute harus diantara :min dan :max digit.',
    'email'                => ':attribute harus berisi e-mail yang valid.',
    'filled'               => ':attribute diwajibkan.',
    'exists'               => 'Pilihan :attribute tidak tepat.',
    'image'                => ':attribute haruslah berupa gambar/citra digital.',
    'in'                   => 'Pilihan dalam :attribute tidak tepat.',
    'integer'              => ':attribute harus berupa bilangan bulat.',
    'ip'                   => ':attribute harus berisi alamat IP yang benar.',
    'max'                  => [
        'numeric' => ':attribute tidak boleh melebihi :max.',
        'file'    => ':attribute tidak boleh melebihi :max KB.',
        'string'  => ':attribute tidak boleh melebihi :max karakter.',
        'array'   => ':attribute tidak boleh berisi lebih dari :max item.',
    ],
    'mimes'                => ':attribute seharusnya berisi file: :values.',
    'min'                  => [
        'numeric' => ':attribute setidaknya bernilai :min.',
        'file'    => ':attribute setidaknya berukuran :min KB.',
        'string'  => ':attribute minimal berisi :min karakter.',
        'array'   => ':attribute harus memiliki minimal :min item.',
    ],
    'not_in'               => 'Pilihan pada :attribute tidak benar.',
    'numeric'              => ':attribute harus berupa angka.',
    'regex'                => ':attribute format tidak sesuai.',
    'required'             => ':attribute diperlukan.',
    'required_if'          => ':attribute diperlukan bila :other bernilai :value.',
    'required_with'        => ':attribute diperlukan :values ada.',
    'required_with_all'    => ':attribute diperlukan :values ada.',
    'required_without'     => ':attribute diperlukan :values tidak ada.',
    'required_without_all' => ':attribute harus diisi apabila :values tidak ada.',
    'same'                 => ':attribute dan :other harus cocok.',
    'size'                 => [
        'numeric' => ':attribute harus sebesar :size.',
        'file'    => ':attribute seharusnya berukuran :size KB.',
        'string'  => ':attribute hanya boleh :size karakter.',
        'array'   => ':attribute harus memiliki :size item.',
    ],
    'string'               => ':attribute haruslah berupa string.',
    'timezone'             => ':attribute harus zona yang valid.',
    'unique'               => ':attribute sudah digunakan.',
    'url'                  => 'format :attribute salah.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'maaf, masukan anda tidak dapat kami validasi',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
