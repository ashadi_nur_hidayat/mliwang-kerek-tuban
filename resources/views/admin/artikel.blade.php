@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') @if($isEdit) {{ $Post->title }} @else Buat Artikel Baru @endif @endsection
@section('css') @endsection
@section('js')
  @include('admin.layouts.tinymce-script')
  @include('admin.layouts.image-refresh')
@endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<form class="form-horizontal" method="post" action="@if($isEdit && ($Post->id != null)){{ route('admin.article.update', $Post->id) }}@else{{ route('admin.article.store')}}@endif ">
  {!! csrf_field() !!}
  @if($isEdit && ($Post->id != null))
  {!! method_field('put') !!}
  @endif
  <div class="form-group">
    <label class="col-md-2 control-label" for="judul">Judul Artikel</label>
    <div class="col-md-10">
      <input id="judul" class="form-control" type="text" name="title" value="@if($isEdit){{ $Post->title }}@endif" placeholder="Masukkan judul artikel" required/>
    </div>
  </div>
  <div class="form-group">
    <label class="col-md-2 control-label" for="kategori">Kategori Artikel</label>
    <div class="col-md-10">
      <select class="form-control" id="kategori" name="category" required>
        @foreach($Categories as $Category)
         <option value="{{ $Category->id }}" @if($isEdit) @if($Category->id == $Post->Category->id){{ 'selected' }}@endif @endif>{{ $Category->name }}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-md-2 control-label" for="sampul">Gambar Sampul</label>
    <div class="col-md-10">
      <select class="form-control" id="sampul" name="intro_image">
        <option value="-1"> - </option>
        @foreach($Images as $Image)
         <option value="{{ $Image->id }}" @if($isEdit) @if(($Post->Image != null) && ($Image->id == $Post->Image->id)){{ 'selected' }}@endif @endif>{{ $Image->name }}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-md-2 control-label" for="tag">Kata Kunci</label>
    <div class="col-md-10">
      <input id="tag" class="form-control" type="text" name="keywords" value="@if($isEdit){{ $Post->keywords }}@endif" placeholder="Masukkan kata kunci pencarian, pisahkan dengan tanda koma" />
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <textarea class="frontier" id="documentEditor" name="contents">@if($isEdit){{ $Post->contents }}@endif</textarea>
    </div>
  </div>
    <hr>
    <div class="form-group text-right">
      <div class="col-md-12">
        <button type="button" name="RefreshAndReload" class="btn btn-default"><i class="fa fa-refresh"></i> Muat Ulang Relasi</button>
        <input type="submit" class="btn btn-success" value="@if($isEdit){{ 'Simpan' }}@else{{ 'Publikasikan' }}@endif">
      </div>
    </div>
  </div>
</form>
@endsection
@section('pagination') @endsection
