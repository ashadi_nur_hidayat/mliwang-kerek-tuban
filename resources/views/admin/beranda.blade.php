@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Beranda @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')
  <strong>Selamat Datang</strong>, {{ $lUser->realname }} (<i>{{ $lUser->username }}</i>)
  <br>
  Pada administrasi website Desa Mliwang.
@endsection
@section('pagination') @endsection
