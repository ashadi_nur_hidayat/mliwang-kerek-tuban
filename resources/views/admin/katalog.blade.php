@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') @if($isEdit && isset($Catalog->id)) Sunting Katalog @else Buat Katalog Baru @endif @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<form class="form-horizontal" action="@if($isEdit && ($Catalog->id != null)){{ route('admin.catalog.update', $Catalog->id) }}@else{{ route('admin.catalog.store') }}@endif" method="post">
  {!! csrf_field() !!}
  @if($isEdit && ($Catalog->id != null))
  {!! method_field('put') !!}
  @endif
  <div class="form-group">
    <label for="name" class="col-md-2 control-label">Nama</label>
    <div class="col-md-10">
      <input type="text" name="name" value="@if($isEdit){{ $Catalog->name }}@endif" class="form-control" id="name" required>
    </div>
  </div>
  <div class="form-group text-right">
    <div class="col-md-12">
      <input type="submit" value="@if($isEdit){{ 'Simpan' }}@else{{ 'Tambahkan' }}@endif" class="btn btn-success">
    </div>
  </div>
</form>
@endsection
@section('pagination') @endsection
