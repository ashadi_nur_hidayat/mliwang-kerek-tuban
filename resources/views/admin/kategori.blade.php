@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') @if($isEdit && isset($Category->id)) Perbarui Kategori @else Buat Kategori Baru @endif @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<form class="form-horizontal" action="@if($isEdit && ($Category->id != null)){{ route('admin.category.update', $Category->id) }}@else{{ route('admin.category.store') }}@endif" method="post">
  {!! csrf_field() !!}
  @if($isEdit && ($Category->id != null))
  {!! method_field('put') !!}
  @endif
  <div class="form-group">
    <label for="name" class="col-md-2 control-label">Nama</label>
    <div class="col-md-10">
      <input type="text" name="name" value="@if($isEdit){{ $Category->name }}@endif" class="form-control" id="name" required>
    </div>
  </div>
  <div class="form-group">
    <label for="featured" class="col-md-2 control-label">Tampilkan di Halaman Utama</label>
    <div class="col-md-10">
      <select class="form-control" name="featured" class="form-control" id="featured" required>
        <option value="0">Tidak</option>
        <option value="1"@if(($isEdit) && ($Category->featured == 1)){{ 'selected' }}@endif>Ya</option>
      </select>
    </div>
  </div>
  <div class="form-group text-right">
    <div class="col-md-12">
      <input type="submit" value="@if($isEdit){{ 'Simpan' }}@else{{ 'Tambahkan' }}@endif" class="btn btn-success">
    </div>
  </div>
</form>
@endsection
@section('pagination') @endsection
