<script src="{{ asset('vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/js/tinymce-init.js') }}"></script>
<script>
  tinyMCE_settings.image_list = "{{ route('ajax.images.list') }}";
</script>
