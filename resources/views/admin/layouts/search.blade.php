<div class="row" style="margin-bottom: 20px">
  <div class="col-md-4 col-md-offset-8 col-sm-6 col-sm-offset-6">
    <form class="form-horizontal" action="{{ $searchURL }}" method="post">
      <div class="input-group">
        <input type="text" name="term" class="form-control">
        <div class="input-group-btn">
          <button type="submit" class="btn btn-default">
            &nbsp<span class="glyphicon glyphicon-search"></span>
          </button>
        </div>
      </div>
      {!! csrf_field() !!}
    </form>
  </div>
</div>
