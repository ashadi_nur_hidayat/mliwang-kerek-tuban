@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Artikel @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<?php $searchURL = route('admin.article.search'); ?>
@include('admin.layouts.search')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<div class="table-responsive">
  <table class="table table-hover">
    <colgroup>
      <col span="1" style="width: 5%;"></col>
      <col span="1" style="width: 45%;"></col>
      <col span="1" style="width: 20%"></col>
      <col span="1" style="width: 20%"></col>
      <col span="1" style="width: 5%;"></col>
      <col span="1" style="width: 5%;"></col>
    </colgroup>
    <tr>
      <th>No</th>
      <th>Judul Artikel</th>
      <th>Kategori</th>
      <th>Penyunting Terakhir</th>
      <th></th>
      <th></th>
    </tr>
    <?php $iCounter = 1 ?>
    @foreach($Posts as $Post)
    <tr>
      <td>{{ $iCounter++ }}</td>
      <td><a href="{{ route('category.article.show', [$Post->Category->slug, $Post->slug]) }}" target="_blank">{{ $Post->title }}</a></td>
      <td><a href="{{ route('admin.category.show', $Post->Category->id) }}">{{ $Post->Category->name }}</a></td>
      <td>{{ $Post->User->realname }}</td>
      <td><a class="btn btn-default" href="{{ route('admin.article.edit', $Post->id) }}">Sunting</a></td>
      <td><a class="btn btn-danger" data-toggle="modal" data-target="#confirmation{{ $Post->id }}">Hapus</a></td>
    </tr>
    @endforeach
  </table>
</div>
<!-- Modal -->
@foreach($Posts as $Post)
<div class="modal fade" id="confirmation{{ $Post->id }}" tabindex="-1" role="dialog" aria-labelledby="confirmationLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="confirmationLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus artikel <strong>{{$Post->title}}</strong>?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('admin.article.destroy', $Post->id) }}" method="post">
          {!! csrf_field() !!}
          {!! method_field('delete') !!}
          <input type="submit" value="Hapus" class="btn btn-danger">
          <input type="reset"  value="Batal" class="btn btn-default" data-dismiss="modal">
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
@section('pagination') {!! $Posts->render() !!} @endsection
