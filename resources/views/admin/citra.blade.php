@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Manajemen Citra @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<div class="row">
  <div class="col-md-6">
    <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#tambahGambar">Tambah Gambar</a>
  </div>
  <div class="col-md-6">
    <?php $searchURL = route('admin.image.search') ?>
      <form class="form-horizontal" action="{{ $searchURL }}" method="post">
        <div class="input-group">
          <input type="text" name="term" class="form-control">
          <div class="input-group-btn">
            <button type="submit" class="btn btn-default">
              &nbsp<span class="glyphicon glyphicon-search"></span>
            </button>
          </div>
        </div>
        {!! csrf_field() !!}
      </form>
  </div>
</div>

<hr>

<div class="modal fade" id="tambahGambar" tabindex="-1" role="dialog" aria-labelledby="tambahGambarLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tambahGambarLabel">Tambah Gambar</h4>
      </div>
      <div class="modal-body">
        <form class="" action="{{ route('admin.image.store') }}" method="post" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <div class="form-group">
            <label for="nama">Nama Gambar</label>
            <input type="text" class="form-control" name="name" id="nama" placeholder="Nama Gambar" required>
          </div>
          <div class="form-group">
            <label for="deskripsi">Deskripsi</label>
            <textarea id="deskripsi" name="description" class="form-control" name="deskripsi" rows="8" cols="40" placeholder="Deskripsi Gambar"></textarea>
          </div>
          <div class="form-group">
            <label for="gambar">File <small>(maks. 2 MB dan resolusi 1500px x 1500px)</small></label>
            <input type="file" accept="image/*" name="file" id="gambar" required>
          </div>
          <hr>
          <div class="text-right">
            <input type="submit" value="Simpan" class="btn btn-success">
            <input type="reset" value="Batal" class="btn btn-danger" data-dismiss="modal">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="media">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  @foreach($Images as $Image)
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="heading{{ $Image->id }}">
        <a href="#collapse{{ $Image->id }}" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapse{{ $Image->id }}">
          <h4 class="panel-title">
            {{ $Image->name }}
          </h4>
        </a>
      </div>
      <div id="collapse{{ $Image->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $Image->id }}">
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-hover">
              <colgroup>
                <col span="1" style="width: 30%;"></col>
                <col span="1" style="width: 70%;"></col>
              </colgroup>
              <tr>
                <td><b>Nama</b></td>
                <td>{{ $Image->name }}</td>
              </tr>
              <tr>
                <td><b>Deskripsi</b></td>
                <td>{{ $Image->description }}</td>
              </tr>
              <tr>
                <td><b>Nama File</b></td>
                <td>{{ $Image->image_path }}</td>
              </tr>
              <tr>
                <td><b>URL</b></td>
                <td><a href="{{ route('images', $Image->image_path) }}" target="_blank">{{ route('images', $Image->image_path) }}</a></td>
              </tr>
              <tr>
                <td><b>MIME</b></td>
                <td>{{ $Image->mime }}</td>
              </tr>
              {{--<tr>
                <td><b>Ukuran Gambar (lebar x tinggi)</b></td>
                <td>{{ $Image->width }} x {{ $Image->height }}</td>
              </tr>--}}
              <tr>
                <td><b>Ukuran File (Kbyte)</b></td>
                <td>{{ ceil($Image->filesize/1024) }}</td>
              </tr>
              <tr>
                <td><b>Ditambahkan pada</b></td>
                <td>{{ $Image->created_at->format('D, d-m-Y') }}</td>
              </tr>
              <tr>
                <td><b>Diperbarui pada</b></td>
                <td>{{ $Image->updated_at->format('D, d-m-Y') }}</td>
              </tr>
            </table>
            <div class="text-right">
              <a href="#" class="btn btn-default" data-toggle="modal" data-target="#editGambar{{ $Image->id }}">Sunting Rincian</a>
              <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#hapusGambar{{ $Image->id }}">Hapus</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="editGambar{{ $Image->id }}" tabindex="-1" role="dialog" aria-labelledby="suntingGambarLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="suntingGambarLabel">Sunting Gambar</h4>
          </div>
          <div class="modal-body">
            <form class="" action="{{ route('admin.image.update', $Image->id) }}" method="post" enctype="multipart/form-data">
              {!! csrf_field() !!}
              {!! method_field('put') !!}
              <div class="form-group">
                <label for="nama">Nama Gambar</label>
                <input type="text" class="form-control" name="name" id="nama" placeholder="Nama Gambar" value="{{ $Image->name }}" required>
              </div>
              <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <textarea id="deskripsi" name="description" class="form-control" name="deskripsi" rows="8" cols="40" placeholder="Deskripsi Gambar">{{ $Image->description }}</textarea>
              </div>
              <div class="form-group">
                <label for="gambar">File(maks. 2 MB dan resolusi 1500px x 1500px)</label>
                <input type="file" accept="image/*" name="file" id="gambar">
              </div>
              <hr>
              <div class="text-right">
                <input type="submit" value="Simpan" class="btn btn-success">
                <input type="reset" value="Batal" class="btn btn-danger" data-dismiss="modal">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="hapusGambar{{ $Image->id }}" tabindex="-1" role="dialog" aria-labelledby="hapusGambarLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="suntingGambarLabel">Hapus Gambar</h4>
          </div>
          <div class="modal-body">
            <p>
              Apakah anda yakin ingin menghapus <strong>{{ $Image->name }}</strong> ({{ $Image->image_path }})
            </p>
          </div>
          <div class="modal-footer">
            <form class="" action="{{ route('admin.image.destroy', $Image->id) }}" method="post">
              {!! csrf_field() !!}
              {!! method_field('delete') !!}
              <input type="submit" value="Hapus" class="btn btn-danger">
              <input type="reset"  value="Batal" class="btn btn-default" data-dismiss="modal">
            </form>
          </div>
        </div>
      </div>
    </div>
  @endforeach
  </div>
</div>
@endsection
@section('pagination') {!! $Images->render() !!} @endsection
