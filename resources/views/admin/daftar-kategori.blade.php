@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Kategori @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<?php $searchURL = route('admin.category.search'); ?>
@include('admin.layouts.search')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<div class="table-responsive">
  <table class="table table-hover">
    <colgroup>
      <col span="1" style="width: 5%;"></col>
      <col span="1" style="width: 30%;"></col>
      <col span="1" style="width: 25%"></col>
      <col span="1" style="width: 30%"></col>
      <col span="1" style="width: 5%;"></col>
      <col span="1" style="width: 5%;"></col>
    </colgroup>
    <tr>
      <th>No</th>
      <th>Kategori</th>
      <th class="text-center">Jumlah Artikel</th>
      <th class="text-center">Halaman Utama</th>
      <th></th>
      <th></th>
    </tr>
    <?php $iCounter = 1 ?>
    @foreach($Categories as $Category)
    <tr>
      <td>{{ $iCounter++ }}</td>
      <td><a href="{{ route('admin.category.show', $Category->id) }}">{{ $Category->name }}</a></td>
      <td class="text-center">{{ count($Category->Article) }}</td>
      <td class="text-center">@if($Category->featured == 1)<span class="fa fa-check fa-fw"></span>@endif</td>
      <td><a class="btn btn-default" href="{{ route('admin.category.edit', $Category->id) }}">Sunting</a></td>
      <td><a class="btn btn-danger" data-toggle="modal" data-target="#confirmation{{ $Category->id }}">Hapus</a></td>
    </tr>
    @endforeach
  </table>
</div>
<!-- Modal -->
@foreach($Categories as $Category)
<div class="modal fade" id="confirmation{{ $Category->id }}" tabindex="-1" role="dialog" aria-labelledby="confirmationLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="confirmationLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus artikel <strong>{{ $Category->name }}</strong>?</p>
        <p>Sejumlah {{ count($Category->Article) }} artikel juga akan <strong>terhapus secara permanen</strong>!</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('admin.category.destroy', $Category->id) }}" method="post">
          {!! csrf_field() !!}
          {!! method_field('delete') !!}
          <input type="submit" value="Hapus" class="btn btn-danger">
          <input type="reset"  value="Batal" class="btn btn-default" data-dismiss="modal">
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
@section('pagination') {!! $Categories->render() !!} @endsection
