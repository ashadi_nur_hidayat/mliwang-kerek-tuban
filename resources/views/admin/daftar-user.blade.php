@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Daftar Administrator @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<?php $searchURL = route('admin.user.search'); ?>
@include('admin.layouts.search')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<div class="table-responsive">
  <table class="table table-hover">
    <colgroup>
      <col span="1" style="width: 5%;"></col>
      <col span="1" style="width: 35%;"></col>
      <col span="1" style="width: 20%"></col>
      <col span="1" style="width: 20%"></col>
      <col span="1" style="width: 20%;"></col>
    </colgroup>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Jumlah Artikel</th>
      <th>Terakhir Masuk</th>
      <th>Terakhir Keluar</th>
    </tr>
    <?php $iCounter = 1 ?>
    @foreach($Users as $User)
    <tr>
      <td>{{ $iCounter++ }}</td>
      <td>{{ $User->realname }}</td>
      <td>{{ count($User->Article) }}</td>
      <td>{{ $User->last_login->format('D, d-m-Y') }}</td>
      <td>{{ $User->last_logout->format('D, d-m-Y') }}</td>
    </tr>
    @endforeach
  </table>
</div>
@endsection
@section('pagination') {!! $Users->render() !!} @endsection
