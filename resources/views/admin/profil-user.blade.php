@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') {{ $lUser->realname }} @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')
  <div class="row">
    <div class="col-md-2">
      Username:
    </div>
    <div class="col-md-10">
      {{ $lUser->username }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      Nama Lengkap:
    </div>
    <div class="col-md-10">
      {{ $lUser->realname }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      E-mail:
    </div>
    <div class="col-md-10">
      {{ $lUser->email }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      Didaftarkan pada:
    </div>
    <div class="col-md-10">
      {{ $lUser->created_at->format('D, d-m-Y') }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      Diperbarui pada:
    </div>
    <div class="col-md-10">
      {{ $lUser->updated_at->format('D, d-m-Y') }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      Terakhir Login:
    </div>
    <div class="col-md-10">
      {{ $lUser->last_login->format('D, d-m-Y; h:m:s') }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      Terakhir Logout:
    </div>
    <div class="col-md-10">
      {{ $lUser->last_logout->format('D, d-m-Y; h:m:s') }}
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      Jumlah Artikel:
    </div>
    <div class="col-md-10">
      {{ $Post_count }}
    </div>
  </div>
@endsection
@section('pagination') @endsection
