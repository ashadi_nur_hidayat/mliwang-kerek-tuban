@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') @if($isEdit && isset($User->id)) {{ $User->realname }} @else Form Registrasi Admin @endif @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<form class="form-horizontal" action="@if($isEdit && ($User->id != null)){{ route('admin.user.update', $User->id) }}@else{{ route('admin.user.store') }}@endif" method="post">
  {!! csrf_field() !!}
  @if($isEdit && ($User->id != null))
  {!! method_field('put') !!}
  @endif
  <div class="form-group">
    <label for="username" class="control-label col-md-2">Username</label>
    <div class="col-md-10">
      <input type="text" name="username" value="@if($isEdit){{ $User->username }}@endif" class="form-control" id="username" @if($isEdit && ($User->id != null)){{ 'readonly' }}@endif required>
    </div>
  </div>
  @if($isEdit && $User->id !=null)
  <div class="form-group">
    <label for="password" class="control-label col-md-2">@if($isEdit && $User->id != null){{ 'Password Lama' }}@else{{ 'Password' }}@endif</label>
    <div class="col-md-10">
      <input type="password" name="password" value="" class="form-control" id="password">
    </div>
  </div>
  @endif
  <div class="form-group">
    <label for="newPassword" class="control-label col-md-2">Password Baru</label>
    <div class="col-md-10">
      <input type="password" name="newPassword" value="" class="form-control" id="newPassword" @if(!$isEdit){{ 'required' }}@endif>
    </div>
  </div>
  <div class="form-group">
    <label for="newPasswordC" class="control-label col-md-2">Konfirmasi Password Baru</label>
    <div class="col-md-10">
      <input type="password" name="newPasswordC" value="" class="form-control" id="newPasswordC" @if(!$isEdit){{ 'required' }}@endif>
    </div>
  </div>
  <div class="form-group">
    <label for="realname" class="control-label col-md-2">Nama Lengkap</label>
    <div class="col-md-10">
      <input type="text" name="realname" value="@if($isEdit){{ $User->realname }}@endif" class="form-control" id="realname" required>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="control-label col-md-2">E-mail</label>
    <div class="col-md-10">
      <input type="email" name="email" value="@if($isEdit){{ $User->email }}@endif" class="form-control" id="email" @if($isEdit && ($User->id != null)){{ 'readonly' }}@endif required>
    </div>
  </div>
  <div class="text-right">
    <input type="submit" value="@if($isEdit){{ 'Simpan' }}@else{{ 'Daftarkan' }}@endif" class="btn btn-success">
  </div>
</form>
@endsection
@section('pagination') @endsection
