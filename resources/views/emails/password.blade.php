<!DOCTYPE html>
<html lang="id">
  <head>
    <title>Reset Password Admin Mliwang</title>
  </head>
  <body>
    <h1>Reset Password Administrator Web Desa Mliwang</h1>
    <p>
      URL reset password: <a href="{{ route('password.reset.token', $token) }}" target="_blank">{{ route('password.reset.token', $token) }}</a>
    </p>
    <p>
      Dengan mengakses Uniform Resource Locator di atas, saya menyatakan bahwa saya adalah pemilik sah akun e-mail ini dan memang benar-benar pemilik
      akun di website Desa Mliwang. Apabila terbukti saya bukan administrator sah dari website, pemilik website yang terdaftar di PANDI berhak
      menghapus akun saya secara keseluruhan di website Desa Pringgolayan.
    </p>
    <br>
    <br>
    <i>
      Anda <strong>TIDAK PERLU</strong> menjawab e-mail otomatis ini. Terima Kasih.
    </i>
  </body>
</html>
