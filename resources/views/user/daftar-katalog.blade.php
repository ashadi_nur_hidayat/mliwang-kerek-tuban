@extends('user.layouts.master')
@section('title') Katalog Potensi Desa @endsection
@section('meta')
  <meta name="keywords" content="Mliwang,Kerek,Tuban,Katalog Produk">
  <meta name="description" content="{{ 'Katalog Produk Web Desa Mliwang, Kerek' }}">
  <?php
    $fURL         = route('catalog.index');
    $fType        = 'product.group';
    $fTitle       = 'Katalog';
    $fDescription = 'Katalog Produk Web Desa Mliwang, Kerek';
    $fImage       = null;
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css') @endsection
@section('js') @endsection
@section('carousel') @endsection
@section('contents')
<h2>Katalog</h2>
<hr>
<div class="row">
  <div class="col-md-12">
    @foreach($Catalogs as $Catalog)
    <div class="panel panel-default">
      <div class="panel-body">
        <h5><i class="fa fa-briefcase"></i> <a href="{{ route('catalog.show', $Catalog->slug) }}">{{ $Catalog->name }}</a><br></h5>
      </div>
    </div>
    @endforeach
  </div>
</div>
<div class="text-center">
  {!! $Catalogs->render() !!}
</div>
@endsection
