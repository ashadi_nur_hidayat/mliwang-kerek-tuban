@extends('user.layouts.master')
@section('title') Profil Desa @endsection
@section('meta')
  <meta name="keywords" content="Tentang Desa Mliwang, Kerek, Tuban">
  <meta name="description" content="Tentang Desa Mliwang, Kerek, Tuban">
  <?php
    $fURL         = route('root');
    $fType        = 'article';
    $fTitle       = 'Profil Desa';
    $fDescription = 'Tentang Desa Mliwang, Kerek,Tuban';
    if($Demographics->first() != null)
    {
      if($Demographics->first()->Image != null)
      {
        $fImage   = $Demographics->first()->Image->image_path;
      }
      else
      {
        $fImage = null;
      }
    }
    else
    {
      $fImage     = null;
    }
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <!-- Morris Charts CSS -->
  <link href="{{ asset('vendor/morrisjs/morris.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/tentang.css') }}" rel="stylesheet">
@endsection
@section('js')
  <!-- JSSor Slider -->
  <script src="{{ asset('vendor/jssor-slider/js/jssor.js') }}"></script>
  <script src="{{ asset('vendor/jssor-slider/js/jssor.slider.js') }}"></script>
  <script src="{{ asset('assets/js/tentang.js') }}"></script>
  <!-- Morris Charts JavaScript -->
  <script src="{{ asset('vendor/raphael/raphael-min.js') }}"></script>
  <script src="{{ asset('vendor/morrisjs/morris.min.js') }}"></script>
  <!-- Chart Data -->
  <script>
    new Morris.Donut({
    // ID of the element in which to draw the chart.
    element: 'chart-demografik',
    // Chart data records -- each entry in this array corresponds to a point on
    // the chart.
    data: [
      @foreach($Demographics as $Demographic)
      { label: '{{ "$Demographic->name" }}', value: {{ $Demographic->number }} },
      @endforeach
    ],
    resize: true
    });
  </script>
@endsection
@section('carousel')
  @include('user.layouts.carousel-tentang')
@endsection
@section('contents')
<h2>Tentang Mliwang</h2>
<div class="row">
  <div class="col-md-12 text-justify paragraph-indent">
    <p>
            Asal sejarah desa adalah pada pertengahan abad ke 18 paska 
      Perang Pajang yaitu perang besar di tanah Jawa antara 
      bangsawan kesultanan Pajang, banyak bangsawan dan tentara yang 
      melarikan diri dari kerajaan akibat perang saudara. Salah satu 
      daerah pelarian adalah Tuban. Salah satu tempat peristirahatan 
      adalah daerah Kerek. Ada 9 wali yang melakukan perjalanan dan 
      beristirahat ke suatu daerah. Satu wali ke Desa Gaji, 
      sedangkan kedelapan wali lainnya didatangi penduduk untuk 
      dimintai nama kampunt itu. Akhirnya diberikan nama Desa 
      Mliwang. Dalam arti kata, Mliwang adalah delapan wali yang 
      memberikan nama, Tengah adalah wali tersebut berada di 
      tengah-tengah kampung.
    </p>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    @if(count($Authorities) > 0)
    <h3>Perangkat Desa</h3>
    <hr>
    <!-- Perangkat Desa -->
    <?php $c = count($Authorities); ?>
    @for($i = 0; $i < $c; $i++)
    <div class="row">
      @for($j = $i; ($j < $c) && ($j < ($i + 2)); $j++)
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-12 text-center">
            @if($Authorities[$j]->Image != null)
            <img src="{{ route('images', $Authorities[$j]->Image->image_path) }}" alt="{{ $Authorities[$j]->Image->description }}" class="Authority-image" />
            @else
            <img src="{{ route('images', 'noImageP.png') }}" alt="{{ $Authorities[$j]->name }}" class="Authority-image"/>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <strong>{{ $Authorities[$j]->name }}</strong><br>
            <i>{{ $Authorities[$j]->position }}</i>
          </div>
        </div>
      </div>
      @endfor
      <?php $i += 1; ?>
    </div>
    @endfor
    @endif
  </div>
  <div class="col-md-6">
    @if(count($Demographics) > 0)
    <h3>Data Demografi</h3>
    <hr>
    <!-- Chart Pengrajin -->
    <div class="" id="chart-demografik"></div>
    @endif
  </div>
</div>
@endsection
