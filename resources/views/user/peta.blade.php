@extends('user.layouts.master')
@section('title') Peta Desa @endsection
@section('meta')
  <meta name="keywords" content="Mliwang,Kerek,Tuban,Peta">
  <meta name="description" content="Peta Desa Mliwang, Kerek">
  <?php
    $fURL         = route('about.map');
    $fType        = 'article';
    $fTitle       = 'Peta Desa';
    $fDescription = 'Peta Desa Mliwang, Kerek';
    $fImage       = asset('assets/img/map.png');
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <link href="{{ asset('assets/css/tentang.css') }}" rel="stylesheet">
@endsection
@section('js') @endsection
@section('carousel') @endsection
@section('contents')
  <img src="{{ asset('assets/img/map.png') }}" class="img-responsive img-thumbnail" />
@endsection
