{{--
  Required: $Posts contains list of post (preferrably 5 or 10)
--}}

<!-- Slider Begin -->
<div class="hidden-sm hidden-xs" id="slider1_container" style="position: relative; margin: 0 auto; top: -20px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
  <!-- Loading Screen -->
  <div u="loading" style="position: absolute; top: 0px; left: 0px;">
    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
    <div style="position: absolute; display: block; background: url(assets/img/loading.gif) no-repeat center center; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
  </div>
  <!-- Slides Container -->
  <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1300px; height: 500px; overflow: hidden;">
    @foreach($Posts as $Post)
    <div>
      @if($Post->intro_image != null)
      <img u="image" src="{{ route('images', $Post->Image->image_path) }}" alt="{{ $Post->Image->description }}" />
      @else
      <img u="image" src="{{ route('images', 'noImage.png') }}" alt="{{ $Post->title }}" />
      @endif
      <div style="position: absolute; width: 480px; height: 120px; top: 30px; left: 30px; padding: 5px; text-align: left; line-height: 60px; text-transform: uppercase; font-size: 50px; color: #FFFFFF;">
        <span style="background-color: rgba(0, 0, 0, 0.7); padding: 0.5px;">
        {{ str_limit($Post->title, 25) }}
        </span>
      </div>
      <div style="position: absolute; width: 480px; height: 120px; top: 300px; left: 30px; padding: 5px; text-align: left; line-height: 36px; font-size: 30px; color: #FFFFFF; ">
        <span style="background-color: rgba(0, 0, 0, 0.7); padding: 0.5px;">
          {{ str_limit(strip_tags($Post->contents), 45) }}
        </span>
        <br>
        <a href="{{ route('category.article.show', [$Category->slug, $Post->slug]) }}" class="btn btn-primary">Baca Selanjutnya</a>
      </div>
    </div>
    @endforeach
    @if(count($Posts) == 0)
    <div>
      <img u="image" src="{{ route('images', 'noImage.png') }}" alt="Belum ada artikel" />
    </div>
    @endif
  </div>

  <!--#region Bullet Navigator Skin Begin -->
  <!-- Help: http://www.jssor.com/development/slider-with-bullet-navigator-jquery.html -->
  <!-- bullet navigator container -->
  <div u="navigator" class="jssorb12" style="bottom: 16px; right: 6px;">
    <!-- bullet navigator item prototype -->
    <div u="prototype"></div>
  </div>
  <!--#endregion Bullet Navigator Skin End -->

  <!--#region Arrow Navigator Skin Begin -->
  <!-- Help: http://www.jssor.com/development/slider-with-arrow-navigator-jquery.html -->
  <!-- Arrow Left -->
  <span u="arrowleft" class="jssora12l" style="top: 123px; left: 0px;">
  </span>
  <!-- Arrow Right -->
  <span u="arrowright" class="jssora12r" style="top: 123px; right: 0px;">
  </span>
  <!--#endregion Arrow Navigator Skin End -->
</div>
<!-- Slider End -->
