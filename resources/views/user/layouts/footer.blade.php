<footer class="footer">
  <hr>
  <div class="container">
    <div class="row text-center">
      <div class="col-md-12">
        <small>
          Diberdaya oleh <strong>Pemuda Mliwang</strong>
          <br>
          Didesain oleh <strong>Tim KKN-PPM UGM 2015</strong>
        </small>
      </div>
    </div>
    <div class="row hidden-xs">
      <div class="col-sm-2 col-md-offset-4">
        <div class="sponsor_container">
          <a href="{{ route('root') }}" class="sponsor"><img src="{{ asset('assets/img/logo-kkn-ugm-2015.png') }}" alt="KKN-PPM UGM Tuban 2015" class="center-block footer-sponsor"/></a>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="sponsor_container">
          <a href="https://lppm.ugm.ac.id/info-kkn-ppm/" class="sponsor"><img src="{{ asset('assets/img/logo_KKN-PPM-UGM.png') }}" alt="KKN-PPM UGM" class="center-block footer-sponsor"/></a>
        </div>
      </div>
    </div>
  </div>
</footer>
