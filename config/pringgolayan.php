<?php
/**
*   Pringgolayan Configuration, to be user all over the code.. lol
*/
return [
  /**
  *  Social Media URL
  */
  'social'       => [
      'facebook' => [
        'url'    => 'https://www.facebook.com/Kkn-Ppm-UGM-Mliwang-1436888449974596/',
        'name'   => 'Mliwang'
      ],
      'twitter'  => [
        'url'    => 'http://www.twitter.com/',
        'name'   => 'Mliwang'
      ],
      'youtube'  => [
        'url'    => 'https://www.youtube.com/channel/UCUzvK30ZjXgwMBCJ_HQs7QA',
        'name'   => 'Mliwang Channel'
      ],
      'instagram'=> [
        'url'    => 'http://www.instagram.com/',
        'name'   => 'Mliwang'
      ],
      'email'    => [
        'url'    => 'mliwang.kerek.tuban@gmail.com',
        'name'   => 'mliwang.kerek.tuban@gmail.com'
      ],
      'location'    => [
        'url'    => 'https://www.google.com/maps/place/Mliwang,+Kerek,+Tuban+Regency,+East+Java,+Indonesia/@-6.8386869,111.8781124,14z/data=!3m1!4b1!4m2!3m1!1s0x2e77a0b849fee605:0xc4991fd62b75de55',
        'name'   => 'Peta Mliwang'
      ],
      'phone_1'    => [
        'number' => '+62 813 325 558 21',
        'name'   => 'winoto'
      ],
      'phone_2'    => [
        'number' => '+62 856 428 756 43',
        'name'   => 'hadi'
      ]
    ]
];
