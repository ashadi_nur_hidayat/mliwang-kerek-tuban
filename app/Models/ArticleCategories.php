<?php

namespace Pringgolayan\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class ArticleCategories extends Model
{
    use SearchableTrait;
    protected $table     = 'article_categories';
    protected $casts     = ['featured' => 'boolean'];
    protected $timestamp = true;

    protected $searchable = [
      'columns' => [
        'name' => 10
        ]
    ];

    public function Article()
    {
      return $this->hasMany('Pringgolayan\Models\Articles', 'category_id', 'id');
    }

}
