<?php

namespace Pringgolayan\Models;
use Illuminate\Database\Eloquent\Model;

use Nicolaslopezj\Searchable\SearchableTrait;

class Articles extends Model
{
    use SearchableTrait;
    protected $table      ='articles';
    protected $timestamp  = true;

    protected $searchable = [
      'columns' => [
          'title'    => 10,
          'keywords' => 8
        ]
    ];

    public function Category()
    {
      return $this->belongsTo('Pringgolayan\Models\ArticleCategories', 'category_id', 'id');
    }

    public function User()
    {
      return $this->belongsTo('Pringgolayan\Models\User', 'user_id', 'id');
    }

    public function Image()
    {
      return $this->belongsTo('Pringgolayan\Models\Images', 'intro_image', 'id');
    }

}
