<?php

namespace Pringgolayan\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
class ProductCategories extends Model
{
    use SearchableTrait;
    protected $table     = 'product_catalogs';
    protected $timestamp = true;

    protected $searchable = [
      'columns' => [
        'name'  => 10
        ]
    ];

    public function Product()
    {
      return $this->hasMany('Pringgolayan\Models\Products', 'catalog_id', 'id');
    }

}
