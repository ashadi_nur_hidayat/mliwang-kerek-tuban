<?php

namespace Pringgolayan\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Authorities extends Model
{
   protected $table     = 'authorities';
   protected $timestamp = true;
   use SoftDeletes;

   public function Image()
   {
     return $this->belongsTo('Pringgolayan\Models\Images', 'image_id', 'id');
   }

}
