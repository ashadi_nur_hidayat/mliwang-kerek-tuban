<?php

namespace Pringgolayan\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Producers extends Model
{
  use SearchableTrait;
  protected $tables    = 'producers';
  protected $timestamp = true;

  protected $searchable=[
    'columns'   => [
        'name'    => 15,
        'address' => 10
      ]
  ];

  public function Product()
  {
    return $this->belongsToMany('Pringgolayan\Models\Products', 'producers_products', 'producer_id', 'product_id')->withTimestamps();
  }

  public function Image()
  {
    return $this->belongsTo('Pringgolayan\Models\Images', 'image_id', 'id');
  }
}
