<?php

namespace Pringgolayan\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Demographics extends Model
{
    protected $table = 'demographics';
    protected $timestamp = true;

    public function Image()
    {
      return $this->belongsTo('Pringgolayan\Models\Images', 'image_id', 'id');
    }

}
