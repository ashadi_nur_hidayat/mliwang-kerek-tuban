<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Testing Controller
Route::get('test', 'TestingController@index');

//Sitemap Controllers
Route::get('sitemap.xml', [
  //sitemap xml index
  'as'   => 'sitemap.root',
  'uses' => 'User\SitemapController@IndexXML'
  ]);
Route::get('category/sitemap.xml', [
  'as'   => 'sitemap.category',
  'uses' => 'User\SitemapController@CategoryXML'
  ]);
Route::get('catalog/sitemap.xml', [
  'as'   => 'sitemap.catalog',
  'uses' => 'User\SitemapController@CatalogXML'
  ]);
Route::get('about/sitemap.xml', [
  'as'   => 'sitemap.about',
  'uses' => 'User\SitemapController@AboutXML'
  ]);
Route::get('producer/sitemap.xml', [
  'as'   => 'sitemap.producer',
  'uses' => 'User\SitemapController@ProducerXML'
  ]);
Route::get('images/sitemap.xml', [
  'as'   => 'sitemap.image',
  'uses' => 'User\SitemapController@ImageXML'
  ]);

//Common Controllers
Route::get('images/{path}', [
  'as'   => 'images',
  'uses' => 'Common\ImageController@respond'
  ]);

//AJAXs
Route::get('images/', [
  'as'   => 'ajax.images.list',
  'uses' => 'Common\ImageController@jsonList'
  ]);
Route::get('products/', [
  'as'   => 'ajax.product.list',
  'uses' => 'Common\ProductController@jsonList'
  ]);

//FrontEnd Controllers
Route::get('/', [
  'as'   => 'root',
  'uses' => 'User\HomeController@index'
  ]);
Route::post('search/', [
  'as'   => 'search',
  'uses' => 'User\SearchController@index'
  ]);
Route::resource('category', 'User\ArticleCategoryController',[
  'only' => ['index', 'show']
  ]);
Route::resource('category.article', 'User\ArticleController', [
  'only' => ['index', 'show']
  ]);
Route::resource('catalog', 'User\ProductCategoryController', [
  'only' => ['index', 'show']
  ]);
Route::resource('catalog.product', 'User\ProductController', [
  'only' => ['index', 'show']
  ]);
Route::get('about/map', [
  'as'   => 'about.map',
  'uses' => 'User\AboutController@showMap'
  ]);
Route::resource('about', 'User\AboutController', [
  'only' => ['index']
  ]);
Route::resource('producer', 'User\ProducerController', [
  'only' => ['index', 'show']
  ]);

//Reset Password Controllers (accessible by victim)
  // Password reset routes...
  Route::get('password/reset/{token}', [
    'as'  => 'password.reset.token',
    'uses'=> 'Auth\PasswordController@getReset'
  ]);
  Route::post('password/reset', [
    'as'  => 'password.reset.post',
    'uses'=> 'Auth\PasswordController@postReset'
  ]);

//BackEnd Controllers (administrators)
Route::get('admin', [
  'as'   => 'admin.root',
  'uses' => 'Admin\UAuthController@home'
]);
Route::post('admin',[
  'as'   => 'admin.login',
  'uses' => 'Admin\UAuthController@login'
]);
Route::group(['middleware' => 'auth'], function(){
  Route::get('admin/logout',[
    'as'   => 'admin.logout',
    'uses' => 'Admin\UAuthController@logout'
  ]);
  //User Password Reset Controllers
    // Password reset link request routes...
    Route::get('admin/password/email', [
      'as'   => 'admin.password.reset',
      'uses' => 'Auth\PasswordController@getEmail'
    ]);
    Route::post('admin/password/email', [
      'as'   => 'admin.password.reset.post',
      'uses' => 'Auth\PasswordController@postEmail'
    ]);


  //Search Controller hooks
  Route::any('admin/category/search', [
    'as'    => 'admin.category.search',
    'uses'  => 'Admin\ArticleCategoryController@search'
  ]);
  Route::any('admin/catalog/search', [
    'as'    => 'admin.catalog.search',
    'uses'  => 'Admin\ProductCategoryController@search'
  ]);
  Route::any('admin/article/search', [
    'as'    => 'admin.article.search',
    'uses'  => 'Admin\ArticleController@search'
  ]);
  Route::any('admin/product/search', [
    'as'    => 'admin.product.search',
    'uses'  => 'Admin\ProductController@search'
  ]);
  Route::any('admin/user/search', [
    'as'    => 'admin.user.search',
    'uses'  => 'Admin\UserController@search'
  ]);
  Route::any('admin/image/search', [
    'as'    => 'admin.image.search',
    'uses'  => 'Admin\ImageController@search'
  ]);
  Route::any('admin/producer/search', [
    'as'    => 'admin.producer.search',
    'uses'  => 'Admin\ProducerController@search'
  ]);

  //Management Controllers
  Route::resource('admin/category', 'Admin\ArticleCategoryController', [

  ]);
  Route::resource('admin/article', 'Admin\ArticleController', [
    'except' => ['show']
  ]);
  Route::resource('admin/catalog', 'Admin\ProductCategoryController', [

  ]);
  Route::resource('admin/product', 'Admin\ProductController', [
    'except' => ['show']
  ]);
  Route::resource('admin/image', 'Admin\ImageController', [
    'only'   => ['index', 'store', 'update', 'destroy']
  ]);
  Route::resource('admin/authorities', 'Admin\AuthoritiesController',[
    'only'   => ['index', 'store', 'update', 'destroy']
  ]);
  Route::resource('admin/demographics', 'Admin\DemographicController', [
    'only'   => ['index', 'store', 'update', 'destroy']
  ]);
  Route::resource('admin/user', 'Admin\UserController',[
    'except' => ['destroy']
  ]);
  Route::resource('admin/producer', 'Admin\ProducerController', [
    'except' => ['show']
  ]);

});
