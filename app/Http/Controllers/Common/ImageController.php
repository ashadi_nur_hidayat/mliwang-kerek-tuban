<?php

namespace Pringgolayan\Http\Controllers\Common;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Storage;
use Cache;
// use Intervention\Image\ImageManager as Image;
use Pringgolayan\Models\Images as ImageEnt;

class ImageController extends Controller
{
    // protected $ImageCntl;
    // public function __construct(Image $ImageInstance)
    // {
    //   $this->ImageCntl = $ImageInstance;
    // }

    /*
    * For direct 'fast' HTTP Response for image URL
    */
    // protected function respond($name)
    // {
    //   //this code might runs slow.. time out's matters
    //   $path = storage_path('/app/images/') . $name;
    //   //in bytes 2MB
    //   if(Storage::size('images/' . $name) < 2097152)
    //   {
    //     //preview enabled
    //     $outImage = $this->ImageCntl->make($path);
    //     return $outImage->response();
    //   }
    //   else
    //   {
    //     //just download the it out
    //     return Response()->download($path);
    //   }
    // }

    /**
    * Serving images without Image\Intervention
    */
    protected function respond($name)
    {
      $path         = storage_path('/app/images/') . $name;
      if(!Storage::exists('/images/' . $name))
      {
        //if it did not exists in storage, just throw 404
        return abort(404);
      }
      //assume image_path as unique
      $image_entity = ImageEnt::where('image_path', '=', $name)->first();
      if($image_entity != null)
      {
        if($image_entity->mime != null)
        {
          //200 = HTTP OK
          return (new Response(Storage::get('/images/' . $name), 200))->header('Content-type', $image_entity->mime);
        }
      }
      return Response()->download($path);
    }

    public function jsonList(Request $request)
    {
      if($request->ajax())
      {
        //if ajax, do it, else
        $Images = ImageEnt::all();
        $array  = [];
        foreach($Images as $Image)
        {
          $item = new \stdClass();
          $item->id          = $Image->id;
          $item->title       = $Image->name;
          $item->description = $Image->description;
          $item->value       = route('images', $Image->image_path);
          array_push($array, $item);
        }
        return response()->json($array);
      }
      else
      {
        //throw 403
        return abort(403);
      }
    }
}
