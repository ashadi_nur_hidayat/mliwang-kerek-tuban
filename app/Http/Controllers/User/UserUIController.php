<?php
namespace Pringgolayan\Http\Controllers\User;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\ArticleCategories;
use Pringgolayan\Models\ProductCategories;
use Pringgolayan\Models\Articles;
use Pringgolayan\Models\Products;

class UserUIController extends Controller
{
    public function BuildNavbar($inArray)
    {
      $Categories = ArticleCategories::all();
      $Catalogs   = ProductCategories::all();
      $LArticles  = Articles::with('Image', 'Category')->orderBy('updated_at', 'desc')->take(5)->get();
      $LProducts  = Products::with('Image', 'Catalog')->orderBy('updated_at', 'desc')->take(5)->get();
      //sequential additions
      $outArray = array_add($inArray, 'Categories', $Categories);
      $outArray = array_add($outArray, 'Catalogs', $Catalogs);
      $outArray = array_add($outArray, 'LArticles', $LArticles);
      $outArray = array_add($outArray, 'LProducts', $LProducts);
      return $outArray;
    }
}
