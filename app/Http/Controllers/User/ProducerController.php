<?php

namespace Pringgolayan\Http\Controllers\User;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\Producers;
use Pringgolayan\Models\Products;
use Pringgolayan\Models\ProductCategories;
use Pringgolayan\Models\Images;

class ProducerController extends UserUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $Producers = Producers::with('Image')->paginate(12);
      $Producers->setPath(route('producer.index'));
      $array = [
        'Producers' => $Producers
      ];
      $array = $this->buildNavbar($array);
      return view('user.daftar-produsen', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($slug)
    {
      $Producer = Producers::where('slug', '=', $slug)->with('Image')->first();
      if($Producer == null)
      {
        $Producer = Producers::find($slug);
        if($Producer == null)
        {
          abort(404);
        }
      }
      $Products = $Producer->Product()->with('Image', 'Catalog')->paginate(12);
      $Products->setPath(route('producer.show', $slug));
      $array = [
        'Producer' => $Producer,
        'Products' => $Products
      ];
      $array = $this->buildNavbar($array);
      return view('user.produsen', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
