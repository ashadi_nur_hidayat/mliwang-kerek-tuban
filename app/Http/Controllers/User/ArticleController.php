<?php

namespace Pringgolayan\Http\Controllers\User;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\Articles;
use Pringgolayan\Models\ArticleCategories;

class ArticleController extends UserUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($cslug, $aslug)
    {
      $Category = ArticleCategories::where('slug', '=', $cslug)->first();
      if($Category == null)
      {
        //if i found nothing, fall back to simplest method
        $Category = ArticleCategories::find($cslug);
        if($Category == null)
        {
          //had no result
          return abort(404);
        }
      }
      $Post  = Articles::where('slug', '=', $aslug)->with('Image')->first();
      if($Post == null)
      {
        //fall back method
        $Post = Articles::find($aslug);
        if($Post == null)
        {
          //still no result, throws 404
          return abort(404);
        }
      }
      if($Post->category_id != $Category->id)
      {
        //well, it's illegal to access this way anyway
        //but fortunately Categories::where('slug', '=', $cslug)->Article()::where('slug', '=', $aslug)->first()
        //might end call something on 'null pointer' become pointless!
        return abort(404);
      }
      $array = [
        'Category' => $Category,
        'Post'     => $Post
      ];
      $array = $this->BuildNavbar($array);
      return view('user.artikel', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
