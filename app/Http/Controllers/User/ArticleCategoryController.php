<?php

namespace Pringgolayan\Http\Controllers\User;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\Articles;
use Pringgolayan\Models\ArticleCategories;

class ArticleCategoryController extends UserUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Categories = ArticleCategories::orderBy('name', 'asc')->paginate(10);
        $Categories->setPath(route('category.index'));
        $array = [
          'Categories' => $Categories
        ];
        $array = $this->BuildNavbar($array);
        return view('user.daftar-kategori', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($slug)
    {
      //take input slugs instead of ids (for SEF)
      $Category   = ArticleCategories::where('slug', '=', $slug)->first();
      if($Category == null)
      {
        //fall back method
        $Category   = ArticleCategories::find($slug);
        if($Category == null)
        {
          //fall back and still no result, throw 404 - good luck
          return abort(404);
        }
      }
      $Posts       = $Category->Article()->orderBy('created_at', 'desc')->with('Image')->paginate(5);
      $Posts->setPath(route('category.show', $slug));
      $array = [
        'Category' => $Category,
        'Posts'    => $Posts
      ];
      $array = $this->BuildNavbar($array);
      return view('user.kategori', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
