<?php

namespace Pringgolayan\Http\Controllers\User;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\Articles;
use Pringgolayan\Models\Products;

class SearchController extends UserUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $array =[
          'SearchTerm'     => '-',
          'ArticleResults' => null,
          'ProductResults' => null
        ];
        if($request->has('term'))
        {
          $SearchTerm             = $request->input('term');
          $ArticleResults         = Articles::search($SearchTerm)->with('Category', 'Image')->get();
          $ProductResults         = Products::search($SearchTerm)->with('Catalog', 'Image')->get();
          $array['SearchTerm']    = $SearchTerm;
          $array['ArticleResults']= $ArticleResults;
          $array['ProductResults']= $ProductResults;
          $request->session()->flash('success', 'Ditemukan <strong>' . count($ArticleResults) . ' artikel</strong> dan <strong>' .
                                      count($ProductResults) . ' produk</strong> untuk kata kunci <strong>' . $SearchTerm . '</strong>');
        }
        else
        {
          $request->session()->flash('alert', 'Maaf, terjadi kelalaian');
        }
        $array = $this->buildNavbar($array);
        return view('user.search', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
