<?php

namespace Pringgolayan\Http\Controllers\User;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\ProductCategories;
use Pringgolayan\Models\Products;
use Pringgolayan\Models\ProductImages;

class ProductController extends UserUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($cslug, $pslug)
    {
        $Catalog = ProductCategories::where('slug', '=', $cslug)->first();
        if($Catalog == null)
        {
          $Catalog = ProductCategories::find($cslug);
          if($Catalog == null)
          {
            return abort(404);
          }
        }
        $Product  = Products::with(['Image'], ['Producer' => function($query){
            $query->orderBy('name', 'asc');
          }])->where('slug', '=', $pslug)->first();
        if($Product == null)
        {
          $Product = Products::with(['Image'], ['Producer' => function($query){
              $query->orderBy('name', 'asc');
            }])->find($pslug);
          if($Product == null)
          {
            return abort(404);
          }
        }
        if($Product->catalog_id != $Catalog->id)
        {
          return abort(404);
        }
        $array =[
          'Catalog' => $Catalog,
          'Product'  => $Product
        ];
        $array = $this->BuildNavbar($array);
        return view('user.produk', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
