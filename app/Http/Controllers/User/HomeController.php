<?php

namespace Pringgolayan\Http\Controllers\User;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\Articles;
use Pringgolayan\Models\ArticleCategories;

class HomeController extends UserUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $Category    = ArticleCategories::where('featured', '=', 1)->first();
      //defaults in case no category was selected
      if($Category == null)
      {
        $Category  = ArticleCategories::first();
        if($Category == null)
        {
          //if no categories at all, it's time to mess things up - i mean it
          return abort(404);
        }
      }
      $Posts       = $Category->Article()->orderBy('created_at', 'desc')->with('Image')->paginate(5);
      $Posts->setPath(route('root'));
      $array = [
        'Category' => $Category,
        'Posts'    => $Posts
      ];
      $array = $this->BuildNavbar($array);
      return view('user.beranda', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
