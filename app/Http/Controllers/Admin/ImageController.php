<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Exception;
use Storage;
use Cache;
// use Intervention\Image\ImageManager as Image;

use Pringgolayan\Models\Images;

class ImageController extends AdminUIController
{

  //manually extend, formerly i don't think i should use this to generate views (which requires AdminUIController)
  // protected $ImageInstance;
  // protected $imgDefault_Height;
  // protected $imgDefault_Width;
  // public function __construct(Image $ImageInstance)
  // {
  //   $this->ImageInstance = $ImageInstance;
  //   //1366 768
  //   $this->imgDefault_Height = 768;
  //   $this->imgDefault_Width  = 1366;
  // }

  /**
  * Generic function to preprocess image, save it to storage... and.... sigh
  */
  // protected function postProcess(UploadedFile $imageRaw, $prefName, Request $request)
  // {
  //   $storeAt = storage_path() . '/app/images/';
  //   $tmpAt   = storage_path() . '/app/tmp/';
  //
  //   $originalName = $imageRaw->getClientOriginalName();
  //   $originalExt  = $imageRaw->getClientOriginalExtension();
  //   $newExt       = 'png';
  //   $oldName      = str_slug($prefName);
  //   $newName      = $oldName;
  //
  //   for($itera = 0; Storage::exists('/tmp/' . $newName . '.' . $originalExt) || Storage::exists('/images/' . $newName . '.' . $newExt); $itera++)
  //   {
  //     $newName = $oldName . '_' . $itera;
  //   }
  //
  //   $mime   = $imageRaw->getMimeType();
  //   $imageRaw->move($tmpAt, $newName . '.' . $originalExt);
  //   $filename     = $newName . '.' . $originalExt;
  //   $filesize     = Storage::size('/tmp/' . $filename);
  //   if($filesize < 2097152)
  //   {
  //     try
  //     {
  //       $mImage = $this->ImageInstance->make($tmpAt . $filename);
  //     }
  //     catch(Exception $e)
  //     {
  //       $request->session()->flash('alert', 'Upload berhasil, namun file tidak dikenali!');
  //       goto bypass;
  //     }
  //     $height = $mImage->height();
  //     $width  = $mImage->width();
  //
  //     if(($height > $this->imgDefault_Height) || ($width > $this->imgDefault_Width))
  //     {
  //       //resize if too large but still managable with php
  //       $mImage->resize($this->imgDefault_Width, $this->imgDefault_Height, function($constraint){
  //         $constraint->aspectRatio();
  //       });
  //     }
  //
  //     $mImage->encode($newExt, 75);
  //     $mImage->save($storeAt . $newName . '.' . $newExt, 75);
  //
  //     $filename = $newName . '.' . $newExt;
  //     $mime     = $mImage->mime();
  //     $height   = $mImage->height();
  //     $width    = $mImage->width();
  //     $filesize = $mImage->filesize();
  //   }
  //   else
  //   {
  //     $request->session()->flash('alert', 'Citra terlalu besar, pemrosesan citra tidak dilakukan. Namun citra tetap bisa digunakan');
  //     bypass:
  //     Storage::copy('/tmp/' . $filename, '/images/' . $filename);
  //     $height = -1;
  //     $width  = -1;
  //   }
  //
  //   Storage::delete('/tmp/' . $newName . '.' . $originalExt);
  //   return [
  //     'filename' => $filename,
  //     'mime'     => $mime,
  //     'height'   => $height,
  //     'width'    => $width,
  //     'filesize' => $filesize,
  //   ];
  //
  // }


  /**
  * Image Preprocessing, no, now just fetch several file descriptor like mime and size
  * but still, won't dump the tmp folder
  */
  protected function postProcess(UploadedFile $imageRaw, $prefName, Request $request)
  {
    $storeAt = storage_path() . '/app/images/';
    $tmpAt   = storage_path() . '/app/tmp/';

    $originalName = $imageRaw->getClientOriginalName();
    $originalExt  = $imageRaw->getClientOriginalExtension();
    $newExt       = 'png';
    $oldName      = str_slug($prefName);
    $newName      = $oldName;

    for($itera = 0; Storage::exists('/tmp/' . $newName . '.' . $originalExt) || Storage::exists('/images/' . $newName . '.' . $newExt); $itera++)
    {
      $newName = $oldName . '_' . $itera;
    }

    $mime   = $imageRaw->getMimeType();
    $imageRaw->move($tmpAt, $newName . '.' . $originalExt);
    $filename     = $newName . '.' . $originalExt;
    $filesize     = Storage::size('/tmp/' . $filename);

    Storage::copy('/tmp/' . $filename, '/images/' . $filename);
    // $height = -1;
    // $width  = -1;

    Storage::delete('/tmp/' . $newName . '.' . $originalExt);
    return [
      'filename' => $filename,
      'mime'     => $mime,
      // 'height'   => $height,
      // 'width'    => $width,
      'filesize' => $filesize,
    ];

  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $Images = Images::paginate(10);
      $Images->setPath(route('admin.image.index'));
      $array = [
        'Images'  => $Images
      ];
      $array = $this->buildNavbar($array);
      return view('admin.citra', $array);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  Request  $request
   * @return Response
   */
  public function store(Request $request)
  {
      if(($request->hasFile('file')) && ($request->file('file')->isValid()) && ($request->has('name')))
      {
        $oldImage = Images::where('name', '=', $request->input('name'))->first();
        if($oldImage == null)
        {
          $name         = $request->input('name');
          $description  = $request->input('description');

          $img = new Images;
          $img->name        = $name;
          $img->description = $description;

          $outs = $this->postProcess($request->file('file'), $name, $request);
          $img->image_path  = $outs['filename'];
          $img->mime        = $outs['mime'];
          // $img->width       = $outs['width'];
          // $img->height      = $outs['height'];
          $img->filesize    = $outs['filesize'];
          $img->save();
          $request->session()->flash('success', 'Gambar <strong>' . $name . '</strong> berhasil disimpan');
        }
        else
        {
          $request->session()->flash('alert', 'Gambar dengan nama <strong>' . $oldImage->name .'</strong> yang sama sudah ada');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Data tidak lengkap, gambar gagal disimpan');
      }
      return redirect(route('admin.image.index'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  Request  $request
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    if($request->has('name'))
    {
      $oldImage = Images::where('name', '=', $request->input('name'))->first();
      if(($oldImage == null) || ($oldImage->id == $id))
      {
        $name         = $request->input('name');
        $description  = $request->input('description');

        $img = Images::find($id);
        if($img != null)
        {
          $img->name        = $name;
          $img->description = $description;
          if($request->hasFile('file') && $request->file('file')->isValid())
          {
            if(Storage::exists('images/' . $img->image_path))
            {
              Storage::delete('images/' . $img->image_path);
            }
            $outs = $this->postProcess($request->file('file'), $name, $request);
            $img->image_path  = $outs['filename'];
            $img->mime        = $outs['mime'];
            // $img->width       = $outs['width'];
            // $img->height      = $outs['height'];
            $img->filesize    = $outs['filesize'];
          }
          $img->update();
          $request->session()->flash('success', 'Gambar <strong>' . $name . '</strong> berhasil diperbarui');
        }
        else
        {
          $request->session()->flash('alert', 'Terjadi kesalahan, gambar <strong>' . $name . '</strong> tidak dapat diakses');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Gambar dengan nama <strong>' . $oldImage->name .'</strong> yang sama sudah ada');
      }
    }
    else
    {
      $request->session()->flash('alert', 'Data tidak lengkap, entri gambar tidak dapat kami proses');
    }
    return redirect(route('admin.image.index'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id, Request $request)
  {
      $dImage = Images::find($id);
      if($dImage != null)
      {
        if(Storage::exists('images/' . $dImage->image_path))
        {
          Storage::delete('images/' . $dImage->image_path);
        }

        foreach($dImage->Article()->get() as $Article)
        {
          $Article->Image()->dissociate();
          $Article->update();
        }

        foreach($dImage->Authority()->get() as $Authority)
        {
          $Authority->Image()->dissociate();
          $Authority->update();
        }

        foreach($dImage->Demography()->get() as $Demography)
        {
          $Demography->Image()->dissociate();
          $Demography->update();
        }

        foreach($dImage->Producer()->get() as $Producer)
        {
          $Producer->Image()->dissociate();
          $Producer->update();
        }

        //sever relations
        $dImage->Product()->detach();

        $dImage->delete();
        $request->session()->flash('success', 'Gambar telah dihapus');
      }
      else
      {
        $request->session()->flash('alert', 'Terjadi kesalahan, gambar gagal dihapus secara permanen');
      }
      return redirect(route('admin.image.index'));
  }

  public function search(Request $request)
  {
      $array =[
        'SearchTerm'     => '-',
        'Images'       => []
      ];
      if($request->has('term'))
      {
        $SearchTerm            = $request->input('term');
        $Images                = Images::search($SearchTerm)->paginate(10);
        $Images->setPath(route('admin.image.search'));
        $Images->appends(['term' => $request->input('term')]);
        $array['SearchTerm']   = $SearchTerm;
        $array['Images']       = $Images;
        $request->session()->flash('success', 'Ditemukan <strong>' . $Images->total() . ' citra </strong> dengan' .
                                    ' kata kunci <strong>' . $SearchTerm . '</strong>');
      }
      else
      {
        return redirect(route('admin.image.index'));
      }
      $array = $this->buildNavbar($array);
      return view('admin.citra', $array);
  }

}
