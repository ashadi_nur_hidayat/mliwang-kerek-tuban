<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\Products;
use Pringgolayan\Models\ProductCategories;
use Pringgolayan\Models\Images;

class ProductController extends AdminUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Products = Products::with('Catalog', 'Image')->paginate(20);
        $Products->setPath(route('admin.product.index'));
        $array = [
          'Products' => $Products
        ];
        $array = $this->buildNavbar($array);
        return view('admin.daftar-produk', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $array = [
          'isEdit'   => false,
          'Catalogs' => ProductCategories::all(),
          'Images'   => Images::all()
        ];
        $array = $this->buildNavbar($array);
        return view('admin.produk', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $name        = $request->input('name');
        $catalog     = $request->input('catalog');
        $slug        = str_slug($name);
        $description = $request->input('description');

        $nProduct = new Products;
        $nProduct->name = $name;
        $nProduct->catalog_id = $catalog;
        $nProduct->slug = $slug;
        $nProduct->description = $description;

        if($request->has('name') && $request->has('catalog'))
        {
          $oProduct = Products::where('slug', '=', $slug)->first();
          if($oProduct == null)
          {
            $nProduct->save();
            $request->session()->flash('success', 'Produk <strong>' . $name . '</strong> telah disimpan!');
            return redirect(route('admin.product.index'));
          }
          else
          {
            $request->session()->flash('alert', 'Produk dengan nama yang sama sudah ada');
          }
        }
        else
        {
          $request->session()->flash('alert', 'Data tidak lengkap, produk tidak dapat disimpan!');
        }
        $array = [
          'isEdit'  => true,
          'Product' => $nProduct,
          'Images'  => Images::all(),
          'Catalogs'=> ProductCategories::all()
        ];
        $array = $this->buildNavbar($array);
        return view('admin.produk', $array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $Product = Products::with('Image', 'Catalog')->where('id', '=', $id)->first();
      if(!$Product)
      {
        abort(404);
      }

      $array = [
        'isEdit'   => true,
        'Product'  => $Product,
        'Catalogs' => ProductCategories::all(),
        'Images'   => Images::all()
      ];
      $array = $this->buildNavbar($array);
      return view('admin.produk', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
      $name        = $request->input('name');
      $catalog     = $request->input('catalog');
      $slug        = str_slug($name);
      $description = $request->input('description');
      $imagesArr   = [];
      if($request->has('image'))
      {
        $imagesArr   = $request->input('image');
      }

      $nProduct = Products::find($id);
      if($nProduct != null)
      {
        if($request->has('name') && $request->has('catalog'))
        {
          $nProduct->name = $name;
          $nProduct->catalog_id = $catalog;
          $nProduct->slug = $slug;
          $nProduct->description = $description;

          $oProduct = Products::where('slug', '=', $slug)->first();
          if(($oProduct == null) || ($oProduct->id == $nProduct->id))
          {
            //attach-ing image data (by detaching everything else, hope i don't exhaust ids quickly...)
            $nProduct->Image()->detach();
            foreach($imagesArr as $img)
            {
              if($img != -1)
              {
                $nProduct->Image()->attach($img);
              }
            }

            $nProduct->update();
            $request->session()->flash('success', 'Produk <strong>' . $name . '</strong> telah diperbarui');
            return redirect(route('admin.product.index'));
          }
          else
          {
            $request->session()->flash('alert', 'Nama produk <strong>' . $name . '</strong> telah digunakan');
          }
        }
        else
        {
          $request->session()->flash('alert', 'Data tidak lengkap, belum bisa diproses');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Terjadi kesalahan, gagal memperbarui data produk');
        return redirect(route('admin.product.index'));
      }

      $array = [
        'isEdit'  => true,
        'Product' => $nProduct,
        'Images'  => Images::all(),
        'Catalogs'=> ProductCategories::all()
      ];
      $array = $this->buildNavbar($array);
      return view('admin.produk', $array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $dProduct = Products::find($id);
        if($dProduct != null)
        {
          //sever all connections (images)
          $dProduct->Image()->detach();
          $dProduct->Producer()->detach();
          //remove the root
          $dProduct->delete();
          $request->session()->flash('success', 'Produk telah berhasil dihapus');
        }
        else
        {
          $request->session()->flash('alert', 'Gagal menghapus produk');
        }
        return redirect(route('admin.product.index'));
    }

    public function search(Request $request)
    {
        $array =[
          'SearchTerm'     => '-',
          'Products'       => []
        ];
        if($request->has('term'))
        {
          $SearchTerm               = $request->input('term');
          $ProductResults           = Products::search($SearchTerm)->with('Catalog', 'Image')->paginate(20);
          $ProductResults->setPath(route('admin.product.search'));
          $ProductResults->appends(['term' => $request->input('term')]);
          $array['SearchTerm']      = $SearchTerm;
          $array['Products']        = $ProductResults;
          $request->session()->flash('success', 'Ditemukan <strong>' . $ProductResults->total() . ' produk </strong> dengan' .
                                      ' kata kunci <strong>' . $SearchTerm . '</strong>');
        }
        else
        {
          return redirect(route('admin.product.index'));
        }
        $array = $this->buildNavbar($array);
        return view('admin.daftar-produk', $array);
    }

}
