<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\Demographics;
use Pringgolayan\Models\Images;

class DemographicController extends AdminUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Demographics = Demographics::with('Image')->paginate(10);
        $Demographics->setPath(route('admin.demographics.index'));
        $array =[
          'Demographics' => $Demographics,
          'Images'       => Images::all()
        ];
        $array = $this->buildNavbar($array);
        return view('admin.demografik', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        if($request->has('name') && $request->has('number'))
        {
          $nDemographic         = new Demographics;
          $nDemographic->name   = $request->input('name');
          $nDemographic->number = $request->input('number');
          if($request->input('image') != -1)
          {
            $nDemographic->image_id = $request->input('image');
          }
          $nDemographic->save();
          $request->session()->flash('success', 'Data demografik telah berhasil ditambahkan');
        }
        else
        {
          $request->session()->flash('alert', 'Data demografik tidak lengkap, data tidak dapat kami proses');
        }
        return redirect(route('admin.demographics.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $nDemographic         = Demographics::find($id);
      $nDemographic->name   = $request->input('name');
      $nDemographic->number = $request->input('number');
      if($request->input('image') != -1)
      {
        $nDemographic->image_id = $request->input('image');
      }
      else
      {
        $nDemographic->image_id = null;
      }

      if($nDemographic != null)
      {
        if($request->has('name') && $request->has('number'))
        {
          $nDemographic->update();
          $request->session()->flash('success', 'Data demografik telah berhasil ditambahkan');
        }
        else
        {
          $request->session()->flash('alert', 'Data demografik tidak lengkap, data tidak dapat kami proses');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Terjadi kesalahan, tidak dapat memperbarui data demografik');
      }
      return redirect(route('admin.demographics.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
      $dDemographics = Demographics::find($id);
      if($dDemographics != null)
      {
        $dDemographics->delete();
        $request->session()->flash('success', 'Data demografik telah kami hapus');
      }
      else
      {
        $request->session()->flash('alert', 'Entah kenapa tidak dapat menghapus data');
      }
      return redirect(route('admin.demographics.index'));
    }
}
