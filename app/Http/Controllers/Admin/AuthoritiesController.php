<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use DB;

use Pringgolayan\Models\Authorities;
use Pringgolayan\Models\Images;

class AuthoritiesController extends AdminUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $array = [
          'Authorities' => Authorities::with('Image')->get(),
          'Images'      => Images::all()
        ];
        $array = $this->buildNavbar($array);
        return view('admin.perangkat-dusun', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if($request->has('name') && $request->has('position'))
        {
          $nAuthority           = new Authorities;
          $nAuthority->name     = $request->input('name');
          $nAuthority->position = $request->input('position');
          $nAuthority->order    = DB::table('authorities')->whereNull('deleted_at')->max('order') + 1;
          if($request->input('image') != -1)
          {
            $nAuthority->image_id = $request->input('image');
          }
          $nAuthority->save();
          $request->session()->flash('success', 'Data telah kami simpan');
        }
        else
        {
          $request->session->flash('alert', 'Data tidak lengkap, sehingga tidak dapat kami proses');
        }
        return redirect(route('admin.authorities.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
      if($request->has('name') && $request->has('position'))
      {
        $nAuthority           = Authorities::find($id);
        if($nAuthority != null)
        {
          $nAuthority->name     = $request->input('name');
          $nAuthority->position = $request->input('position');
          $nAuthority->order    = $request->input('order');
          if($request->input('image') != -1)
          {
            $nAuthority->image_id = $request->input('image');
          }
          else
          {
            $nAuthority->image_id = null;
          }
          $nAuthority->update();
          $request->session()->flash('success', 'Data telah kami perbarui');
        }
        else
        {
          $request->session()->flash('alert', 'Terjadi kesalahan dalam menyimpan data');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Data belum lengkap, sehingga tidak dapat kami simpan');
      }
      return redirect(route('admin.authorities.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
      $dAuthority = Authorities::find($id);
      if($dAuthority != null)
      {
        $dAuthority->delete();
        $request->session()->flash('success', 'Data telah kami hapus');
      }
      else
      {
        $request->session()->flash('alert', 'Entah kenapa tidak dapat menghapus data');
      }
      return redirect(route('admin.authorities.index'));
    }
}
