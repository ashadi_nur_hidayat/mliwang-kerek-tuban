<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\ProductCategories;
use Pringgolayan\Models\Products;

class ProductCategoryController extends AdminUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Catalogs = ProductCategories::with('Product')->paginate(10);
        $Catalogs->setPath(route('admin.catalog.index'));
        $array = [
          'Catalogs' => $Catalogs
        ];
        $array = $this->buildNavbar($array);
        return view('admin.daftar-katalog', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $array = [
          'isEdit' => false
        ];
        $array = $this->buildNavbar($array);
        return view('admin.katalog', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $name = $request->input('name');
      $slug = str_slug($name);

      $nCat = new ProductCategories;
      $nCat->name = $name;
      $nCat->slug = $slug;
      if($request->has('name'))
      {
        $oCat = ProductCategories::where('slug', '=', $slug)->first();
        if($oCat == null)
        {
          $nCat->save();
          $request->session()->flash('success', 'Katalog <strong>' . $name . '</strong> berhasil disimpan');
          return redirect(route('admin.catalog.index'));
        }
        else
        {
          $request->session()->flash('alert', 'Katalog dengan nama yang sama sudah ada');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Data tidak lengkap, katalog belum bisa disimpan');
      }
      $array = [
        'isEdit'  => true,
        'Catalog' => $nCat
      ];
      $array = $this->buildNavbar($array);
      return view('admin.katalog', $array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
      $Products = Products::with('Catalog', 'Image')->where('catalog_id', '=', $id)->paginate(10);
      $Products->setPath(route('admin.product.index'));
      $array = [
        'Products' => $Products
      ];
      $array = $this->buildNavbar($array);
      return view('admin.daftar-produk', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $Catalog = ProductCategories::find($id);
      if(!$Catalog)
      {
        abort(404);
      }
      $array = [
        'isEdit'  => true,
        'Catalog' => $Catalog
      ];
      $array = $this->buildNavbar($array);
      return view('admin.katalog', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
      $name = $request->input('name');
      $slug = str_slug($name);

      $nCat = ProductCategories::find($id);
      if(!$nCat)
      {
        abort(404);
      }

      $nCat->name = $name;
      $nCat->slug = $slug;
      if($nCat != null)
      {
        if($request->has('name'))
        {
          $oCat = ProductCategories::where('slug', '=', $slug)->first();
          if(($oCat == null) || ($oCat->id == $nCat->id))
          {
            $nCat->update();
            $request->session()->flash('succcess', 'Katalog <strong>' . $name . '</strong> telah diperbarui');
            return redirect(route('admin.catalog.index'));
          }
          else
          {
            $request->session()->flash('alert', 'Katalog tidak dapat disimpan, nama katalog <strong>' . $name . '</strong> sudah digunakan');
          }
        }
        else
        {
          $request->session()->flash('alert', 'Form tidak lengkap, katalog <strong>' . $nCat->name . '</strong> belum bisa disimpan');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Terjadi kesalahan, gagal memperbarui katalog');
      }
      $array = [
        'isEdit'  => true,
        'Catalog' => $nCat
      ];
      $array = $this->buildNavbar($array);
      return view('admin.katalog', $array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, ProductController $ProdCInstance, Request $request)
    {
      $dCatalog = ProductCategories::find($id);
      if($dCatalog != null)
      {
        foreach($dCatalog->Product as $Product)
        {
          $ProdCInstance->destroy($Product->id);
        }
        $dCatalog->delete();
        $request->session()->flash('success', 'Katalog beserta produknya telah dihapus');
      }
      else
      {
        $request->session()->flash('alert', 'Terjadi kesalahan, katalog tidak dapat dihapus');
      }
      return redirect(route('admin.catalog.index'));
    }

    public function search(Request $request)
    {
        $array =[
          'SearchTerm'     => '-',
          'Catalogs' => null
        ];
        if($request->has('term'))
        {
          $SearchTerm               = $request->input('term');
          $ProductCategoriesResults = ProductCategories::search($SearchTerm)->with('Product')->paginate(10);
          $ProductCategoriesResults->setPath('admin.catalog.search');
          $ProductCategoriesResults->appends(['term' => $request->input('term')]);
          $array['SearchTerm']      = $SearchTerm;
          $array['Catalogs']        = $ProductCategoriesResults;
          $request->session()->flash('success', 'Ditemukan <strong>' . $ProductCategoriesResults->total() . ' katalog </strong> dengan' .
                                      ' kata kunci <strong>' . $SearchTerm . '</strong>');
        }
        else
        {
          return redirect(route('admin.catalog.index'));
        }
        $array = $this->buildNavbar($array);
        return view('admin.daftar-katalog', $array);
    }

}
