<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Auth;

use Pringgolayan\Models\Articles;
use Pringgolayan\Models\ArticleCategories;
use Pringgolayan\Models\Images;

class ArticleController extends AdminUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Posts    = Articles::with('Image', 'Category', 'User')->paginate(20);
        $Posts->setPath(route('admin.article.index'));
        $array    = [
          'Posts' => $Posts
        ];
        $array = $this->buildNavbar($array);
        return view('admin.daftar-artikel', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $Categories = ArticleCategories::all();
        $Images     = Images::all();
        $array =[
          'isEdit'     => false,
          'Categories' => $Categories,
          'Images'     => $Images
        ];
        $array = $this->buildNavbar($array);
        return view('admin.artikel', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $title          = $request->input('title');
        $category       = $request->input('category');
        $intro_image    = $request->input('intro_image');
        $keywords       = $request->input('keywords');
        $contents       = $request->input('contents');

        $slugName = str_slug($title);

        $nArticle = new Articles;
        $nArticle->title       = $title;
        $nArticle->category_id = $category;
        $nArticle->user_id     = Auth::user()->id;
        $nArticle->slug        = $slugName;
        $nArticle->keywords    = $keywords;
        $nArticle->contents    = $contents;

        if($intro_image != -1)
        {
          //no image
          $nArticle->intro_image = $intro_image;
        }

        if($request->has('title') && $request->has('category'))
        {
          //check for collision
          $oArticle = Articles::where('slug', '=', $slugName)->first();

          //if exist... and if not exist...
          if($oArticle == null)
          {
            $nArticle->save();
            $request->session()->flash('success', 'Artikel <strong>' . $title . '</strong> telah disimpan');
            return redirect(route('admin.article.index'));
          }
          else
          {
            $request->session()->flash('alert', 'Artikel tidak dapat disimpan, sudah ada artikel dengan nama yang sama');
          }
        }
        else
        {
          $request->session()->flash('alert', 'Data tidak lengkap, artikel belum bisa diproses');
        }
        $array = [
          'isEdit'     => true,
          'Post'       => $nArticle,
          'Categories' => ArticleCategories::all(),
          'Images'     => Images::all()
        ];
        $array = $this->buildNavbar($array);
        return view('admin.artikel', $array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $Post       = Articles::with('Image', 'Category')->where('id', '=', $id)->first();
      if($Post == null)
      {
        abort(404);
      }
      $array = [
        'isEdit'     => true,
        'Post'       => $Post,
        'Categories' => ArticleCategories::all(),
        'Images'     => Images::all()
      ];
      $array = $this->buildNavbar($array);
      return view('admin.artikel', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
      $title          = $request->input('title');
      $category       = $request->input('category');
      $intro_image    = $request->input('intro_image');
      $keywords       = $request->input('keywords');
      $contents       = $request->input('contents');

      $slugName = str_slug($title);

      $nArticle = Articles::find($id);
      if($nArticle == null)
      {
        abort(404);
      }
      $nArticle->title       = $title;
      $nArticle->category_id = $category;
      $nArticle->user_id     = Auth::user()->id;
      $nArticle->slug        = $slugName;
      $nArticle->keywords    = $keywords;
      $nArticle->contents    = $contents;
      if($intro_image != -1)
      {
        //set new image as cover
        $nArticle->intro_image = $intro_image;
      }
      else
      {
        //removes old image
        $nArticle->intro_image = null;
      }

      if($nArticle != null)
      {
        if($request->has('title') && $request->has('category'))
        {
          //check for collision
          $oArticle = Articles::where('slug', '=', $slugName)->first();
          //if exist and same... and if not exist...
          if(($oArticle == null) || ($oArticle->id == $nArticle->id))
          {
            $nArticle->save();
            $request->session()->flash('success', 'Artikel <strong>' . $title . '</strong> telah diperbarui');
            return redirect(route('admin.article.index'));
          }
          else
          {
            $request->session()->flash('alert', 'Artikel dengan nama yang sama telah ada, tidak dapat memperbarui artikel');
          }
        }
        else
        {
          $request->session()->flash('alert', 'Artikel tidak dapat disimpan karena data tidak lengkap');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Artikel tidak dapat diperbarui, artikel sebelumnya tidak ditemukan');
      }
      //if exist but differs
      $array = [
        'isEdit'     => true,
        'Post'       => $nArticle,
        'Categories' => ArticleCategories::all(),
        'Images'     => Images::all()
      ];
      $array = $this->buildNavbar($array);
      return view('admin.artikel', $array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $dArticle = Articles::find($id);
        if($dArticle != null)
        {
          $dArticle->delete();
          $request->session()->flash('success', 'Artikel telah dihapus');
        }
        else
        {
          $request->session()->flash('alert', 'Terjadi kesalahan, artikel tidak dapat dihapus');
        }
        return redirect(route('admin.article.index'));
    }

    public function search(Request $request)
    {
        $array =[
          'SearchTerm'     => '-',
          'Posts' => null
        ];
        if($request->has('term'))
        {
          $SearchTerm               = $request->input('term');
          $ArticleResults           = Articles::search($SearchTerm)->with('Image', 'Category', 'User')->paginate(20);
          $ArticleResults->setPath(route('admin.article.search'));
          $ArticleResults->appends(['term' => $request->input('term')]);
          $array['SearchTerm']      = $SearchTerm;
          $array['Posts']           = $ArticleResults;
          $request->session()->flash('success', 'Ditemukan <strong>' . $ArticleResults->total() . ' artikel </strong> dengan' .
                                      ' kata kunci <strong>' . $SearchTerm . '</strong>');
        }
        else
        {
          return redirect(route('admin.article.index'));
        }
        $array = $this->buildNavbar($array);
        return view('admin.daftar-artikel', $array);
    }

}
