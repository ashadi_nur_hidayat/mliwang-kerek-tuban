<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Auth;
use Hash;
use Pringgolayan\Models\User;
use Pringgolayan\Models\Articles;

class UserController extends AdminUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Users = User::with('Article')->paginate(20);
        $Users->setPath(route('admin.user.index'));
        $array = [
          'Users' => $Users
        ];
        $array = $this->buildNavbar($array);
        return view('admin.daftar-user', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $array = [
          'isEdit' => false
        ];
        $array = $this->buildNavbar($array);
        return view('admin.user', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('newPassword');
        $passwordC= $request->input('newPasswordC');
        $realname = $request->input('realname');
        $email    = $request->input('email');

        $nUser           = new User;
        $nUser->username = $username;
        $nUser->password = bcrypt($password);
        $nUser->realname = $realname;
        $nUser->email    = $email;
      if($request->has('username') && $request->has('newPassword') && $request->has('newPasswordC') && $request->has('realname')
         && $request->has('email'))
      {
        $oUser1   = User::where('username', '=', $username)->first();
        $oUser2   = User::where('email', '=', $email)->first();
        if(($oUser1 == null) && ($oUser2 == null))
        {
          if(strcmp($passwordC, $password) == 0)
          {
            $nUser->save();
            $request->session()->flash('success', 'Pengguna <strong>' . $nUser->realname . '</strong> telah ditambahkan');
            return redirect(route('admin.user.index'));
          }
          else
          {
            $request->session()->flash('alert', 'Pasangan password tidak cocok');
            $array = [
              'isEdit' => true,
              'User'   => $nUser,
            ];
            $array = $this->buildNavbar($array);
            return view('admin.user', $array);
          }
        }
        else
        {
          $request->session()->flash('alert', 'User dengan e-mail/username yang sama sudah ada');
          $array = [
            'isEdit' => true,
            'User'   => $nUser,
          ];
          $array = $this->buildNavbar($array);
          return view('admin.user', $array);
        }
      }
      $request->session()->flash('alert', 'Form harus diisi lengkap!');
      $array = [
        'isEdit' => true,
        'User' => $nUser
      ];
      $array = $this->buildNavbar($array);
      return view('admin.user', $array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
      return view('admin.profil-user', $this->buildNavbar(['Post_count' => count(Articles::where('user_id', '=', $id)->get())]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      if($id == Auth::user()->id)
      {
        $array = [
          'isEdit' => true,
          'User'   => User::find($id)
        ];
        $array = $this->buildNavbar($array);
        return view('admin.user', $array);
      }
      else
      {
        return redirect(route('admin.user.index'));
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
      if($request->has('username') && $request->has('realname') && $request->has('email') && (Auth::user()->id == $id))
      {
        $username  = $request->input('username');
        $password  = $request->input('password');
        $Npassword = $request->input('newPassword');
        $NpasswordC= $request->input('newPasswordC');
        $realname  = $request->input('realname');
        $email     = $request->input('email');

        $oUser1   = User::where('username', '=', $username)->first();
        $oUser2   = User::where('email', '=', $email)->first();

        $nUser           = User::find($id);
        if(!$nUser)
        {
          abort(403);
        }

        $nUser->username = $username;
        if(($password != null) && ($Npassword != null) && ($NpasswordC != null))
        {
          //saves password if old password match and new passwords-confirmation pair match
          if((strcmp($Npassword, $NpasswordC) == 0) && (Hash::check($password, $oUser1->password)))
          {
            $nUser->password = bcrypt($Npassword);
          }
          else
          {
            $request->session()->flash('alert', 'Pasangan password tidak cocok');
            $array = [
              'isEdit' => true,
              'User'   => $nUser,
            ];
            $array = $this->buildNavbar($array);
            return view('admin.user', $array);
          }
        }
        $nUser->realname = $realname;
        $nUser->email    = $email;
        if((($oUser1->id == $id) && ($oUser2->id == $id)))
        {
          $nUser->update();
          $request->session()->flash('success', 'Pengguna <strong>' . $nUser->realname . '</strong> telah diperbarui');
          return redirect(route('admin.user.index'));
        }
        else
        {
          $request->session()->flash('alert', 'User dengan e-mail/username yang sama sudah ada');
          $array = [
            'isEdit' => true,
            'User'   => $nUser,
          ];
          $array = $this->buildNavbar($array);
          return view('admin.user', $array);
        }
      }
      $request->session()->flash('alert', 'Form harus diisi lengkap!');
      return redirect(route('admin.user.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {
        $array =[
          'SearchTerm'     => '-',
          'Users'       => []
        ];
        if($request->has('term'))
        {
          $SearchTerm            = $request->input('term');
          $UserResults           = User::search($SearchTerm)->with('Article')->paginate(20);
          $UserResults->setPath(route('admin.user.search'));
          $UserResults->appends(['term' => $request->input('term')]);
          $array['SearchTerm']   = $SearchTerm;
          $array['Users']        = $UserResults;
          $request->session()->flash('success', 'Ditemukan <strong>' . $UserResults->total() . ' pengguna </strong> dengan' .
                                      ' kata kunci <strong>' . $SearchTerm . '</strong>');
        }
        else
        {
          return redirect(route('admin.user.index'));
        }
        $array = $this->buildNavbar($array);
        return view('admin.daftar-user', $array);
    }

}
