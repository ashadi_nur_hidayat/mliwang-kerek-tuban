<?php
namespace Pringgolayan\Sitemap;
use Carbon\Carbon;

class SitemapClassHelper extends \stdClass {
  public $url;        //Escaped urls
  public $lastmod;    //YYYY-MM-DD
  public $changefreq; //always, hourly, weekly monthly yearly never
  public function __construct($url, Carbon $lastmod = null, $changefreq)
  {
    $this->url        = $url;
    if(!$lastmod)
    {
      $lastmod = Carbon::now();
    }
    $this->lastmod    = $lastmod->format('Y-m-d');
    $this->changefreq = $changefreq;
  }
}
