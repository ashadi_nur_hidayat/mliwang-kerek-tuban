//tinyMCE configuration declarations
var tinyMCE_settings = {
  min_height: 500,
  selector: "#documentEditor",
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code",
    "insertdatetime media table contextmenu paste",
  ],
  image_class_list: [
    {title: 'None', value: 'img-responsive'},
    {title: 'Base', value: 'img-responsive post-img'},
    {title: 'Thumbnail', value: 'img-responsive img-thumbnail post-img'},
    {title: 'Round', value: 'img-responsive img-rounded post-img'},
    {title: 'Circle', value: 'img-responsive img-circle post-img'}
  ],
  table_class_list: [
      {title: 'None', value: 'table'},
      {title: 'Table Hoverable', value: 'table table-hover'},
      {title: 'Table Stripped', value: 'table table-stripped'},
      {title: 'Table Bordered', value: 'table table-bordered'},
      {title: 'Table Condensed', value: 'table table-condensed'}
  ],
  //inject bootstrap for preview
  content_css: ['../../../vendor/bootstrap/dist/css/bootstrap.min.css', '../../../vendor/font-awesome/css/font-awesome.min.css'],
  //custom formats to support responsive
  style_formats_merge: true,
  style_formats: [
    {
      title: 'Responsive',
      items: [
        {
          title: 'Embed Responsive',
          format: 'embedresponsive'
        },
        {
          title: 'Table Responsive',
          format: 'tableresponsive'
        }
      ]
    }
  ],
  formats: {
    embedresponsive: {
      block: 'div',
      classes: 'embed-responsive embed-responsive-4by3',
    },
    embedresponsive_item: {
      selector: 'iframe,video,object,embed',
      classes: 'embed-responsive-item'
    },
    tableresponsive: {
      block: 'div',
      classes: 'table-responsive',
    }
  },
  //base callback for <video> tags
  video_template_callback: function(data) {
       return '<div class="embed-responsive embed-responsive-4by3"><video class="embed-responsive-item" width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' +
            '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' +
            (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') +
         '</video></div>';
  },
  toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
  //fixed urls - not good for multiple dev servers
  relative_urls : false,
  remove_script_host : false,
  convert_urls : true,
  image_list: []
};
//initiate tinyMCE
tinymce.init(tinyMCE_settings);
