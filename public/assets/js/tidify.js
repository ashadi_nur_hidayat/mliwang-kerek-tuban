jQuery(document).ready(function ($) {
    //Bootstrapify via JQuery - for postprocessing, in case editor not configured properly.
    var posting = $("#contents");
    posting.find("img").addClass("img-responsive post-img");
    posting.find("iframe").addClass("embed-responsive-item").each(function(index){
      if(!$(this).parent().hasClass('embed-responsive embed-responsive-4by3'))
      {
        $(this).wrap("<div class=\"embed-responsive embed-responsive-4by3\"></div>");
      }
    });
    posting.find("table").addClass("table").each(function(index){
      if(!$(this).parent().hasClass('table-responsive'))
      {
        $(this).wrap("<div class=\"table-responsive\"></div>");
      }
    });
});
